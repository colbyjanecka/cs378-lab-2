
# Branching Scheme:

### create a branch from main, for each feature that we are working on
### IE: a branch for networking, a branch for map development, a branch for adding actors and pawns. etc.

### each team member should be assigned tasks related to different features. if multiple people are working on different aspects of the same feature, they should create different branches

#### branch name scheme should be as follows:
#### feature-subfeature


## following the GitHub flow

##### The main branch is always releasable, and in fact, releases are generally done directly from it.
##### Each developer creates a new branch, the feature branch, for their changes from the main branch.
##### Feature branches can be deployed to a testing environment for verification or pushed directly to the main branch and deployed to a non-production environment from there.
##### A short-lived release branch may be used off the main branch to prepare for and execute a release.

